# encoding: utf-8
"""
Device database models
--------------------
"""

from app.extensions import db


class DeviceDict(db.Model):
    """
    Device-Dict database model.
    """

    id = db.Column(
        db.Integer,
        primary_key=True,autoincrement=True)  # pylint: disable=invalid-name

    type = db.Column(db.String(length=50), default='', nullable=False)
    value = db.Column(db.String(length=50), default='', nullable=False)


class DeviceRecord(db.Model):
    """
    Device-record database model.
    """
    id = db.Column(
        db.Integer,
        primary_key=True,autoincrement=True)  # pylint: disable=invalid-name
    type = db.Column(db.String(length=50), default='', nullable=False)

    accountant_code = db.Column(
        db.String(
            length=50),
        default='',
        nullable=True)

    accountant_name = db.Column(
        db.String(
            length=50),
        default='',
        nullable=False)
    bill_name = db.Column(db.String(length=50), default='', nullable=True)
    code = db.Column(db.Integer())
    description = db.Column(db.String(length=50), default='', nullable=True)
    unit = db.Column(db.String(length=10))
    month = db.Column(db.String(length=20), default='', nullable=True)
    remark = db.Column(db.String(length=200))

    business_date = db.Column(db.Date())

    in_date = db.Column(db.Date())
    in_amount = db.Column(db.Integer())
    in_unit_fee = db.Column(db.Integer())
    in_fee = db.Column(db.Integer())
    in_auth_date = db.Column(db.Date())
    in_provider = db.Column(db.String(length=200))

    sale_date = db.Column(db.Date())
    sale_amount = db.Column(db.Integer())
    sale_unit_fee = db.Column(db.Integer())
    sale_fee = db.Column(db.Integer())
    sale_people = db.Column(db.String(length=200))
    sale_client = db.Column(db.String(length=200))

    balance_date = db.Column(db.Date())
    balance_amount = db.Column(db.Integer())
    balance_unit_fee = db.Column(db.Integer())
    balance_fee = db.Column(db.Integer())
    balance_buy_date = db.Column(db.Date())


class Device(db.Model):
    """
    Device database model.
    """

    id = db.Column(
        db.Integer,
        primary_key=True,autoincrement=True)  # pylint: disable=invalid-name
    code = db.Column(db.Integer())
    accountant_name = db.Column(
        db.String(
            length=50),
        default='',
        nullable=True)
    bill_name = db.Column(db.String(length=50), default='', nullable=True)
    description = db.Column(db.String(length=50), default='', nullable=True)
    unit = db.Column(db.String(length=10))
