# encoding: utf-8
"""
Serialization schemas for Device resources RESTful API
----------------------------------------------------
"""

from flask_restplus_patched import ModelSchema

from .models import Device, DeviceRecord, DeviceDict


class DeviceSchema(ModelSchema):
    """
    Base device schema exposes only the most general fields.
    """

    class Meta:
        # pylint: disable=missing-docstring
        model = Device
        fields = (
            Device.id.key,
            Device.code.key,
            Device.accountant_name.key,
            Device.bill_name.key,
            Device.description.key,
            Device.unit.key,
        )


class DeviceDictSchema(ModelSchema):
    """
    Base device schema exposes only the most general f  ields.
    """

    class Meta:
        # pylint: disable=missing-docstring
        model = DeviceDict
        fields = (
            DeviceDict.id.key,
            DeviceDict.type.key,
            DeviceDict.value.key,
        )


class DeviceRecordSchema(ModelSchema):

    class Meta:
        model = DeviceRecord
        fields = (
            DeviceRecord.id.key,
            DeviceRecord.type.key,
            DeviceRecord.code.key,
            DeviceRecord.accountant_code.key,
            DeviceRecord.accountant_name.key,

            DeviceRecord.bill_name.key,
            DeviceRecord.description.key,
            DeviceRecord.unit.key,
            DeviceRecord.month.key,
            DeviceRecord.remark.key,

            DeviceRecord.in_date.key,
            DeviceRecord.in_amount.key,
            DeviceRecord.in_unit_fee.key,
            DeviceRecord.in_fee.key,
            DeviceRecord.in_auth_date.key,
            DeviceRecord.in_provider.key,

            DeviceRecord.sale_date.key,
            DeviceRecord.sale_amount.key,
            DeviceRecord.sale_unit_fee.key,
            DeviceRecord.sale_fee.key,
            DeviceRecord.sale_people.key,
            DeviceRecord.sale_client.key,

            DeviceRecord.balance_date.key,
            DeviceRecord.balance_amount.key,
            DeviceRecord.balance_unit_fee.key,
            DeviceRecord.balance_fee.key,
            DeviceRecord.balance_buy_date.key,
        )

