# encoding: utf-8
"""
Devices module
============
"""

from app.extensions.api import api_v1


def init_app(app, **kwargs):
    # pylint: disable=unused-argument,unused-variable
    """
    Init devices module.
    """
    api_v1.add_oauth_scope('devices:read', "Provide access to device details")
    api_v1.add_oauth_scope('devices:write', "Provide write access to device details")

    # Touch underlying modules
    from . import models, resources

    api_v1.add_namespace(resources.api)
