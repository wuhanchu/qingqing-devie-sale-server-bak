"""empty message

Revision ID: 2e0fa02c310d
Revises: bc7169a7c8e4
Create Date: 2017-07-06 18:29:01.277363

"""

# revision identifiers, used by Alembic.
revision = '2e0fa02c310d'
down_revision = 'bc7169a7c8e4'

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('device_record', sa.Column('balance_buy_date', sa.Date(), nullable=True))
    op.drop_column('device_record', 'balance_bug_date')
    op.alter_column('team_member', 'is_leader',
               existing_type=mysql.TINYINT(display_width=1),
               type_=sa.Boolean(),
               existing_nullable=False)
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column('team_member', 'is_leader',
               existing_type=sa.Boolean(),
               type_=mysql.TINYINT(display_width=1),
               existing_nullable=False)
    op.add_column('device_record', sa.Column('balance_bug_date', sa.DATE(), nullable=True))
    op.drop_column('device_record', 'balance_buy_date')
    # ### end Alembic commands ###
